pragma solidity ^0.4.23;

contract Shop { 

    
    // ***************
    //  Model 
    // ***************
    
    // define product struct
	
    
    // add a Map to store all products
    

    // ***************
    //  Permissons 
    // ***************

    // add variable to store the owner

    // define modifier for Owner
	


     // ***************
    // Constructor
    // ***************

    constructor () public {
    	// Set owner equals transaction sender
    	
    }

    // ***************
    //  functions
    // ***************

    // Only owner can add product, Check if Owner
    function addProduct(string _name, uint _uid, uint _price, uint _stock ) public isOwner returns (bool success) {
        
       
    }

    // Anyone can buy product  ( reading and writing to the storage costs )

   function buyProduct(uint _productId)  public payable {
     
            
     }	


}
