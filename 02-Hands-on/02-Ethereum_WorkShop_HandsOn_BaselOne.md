
----
# Ethereum WorkShop Hands-on

**1.4.3 Hands-on: Install Geth**

**1.5.5 Hands-on: Create a local Blockchain**

**1.6. Hands-On: Interaction with the Blockchain**
  - 1.6.1 Interaction via Geth JavaScript console
  - 1.6.2 Mining
  - 1.6.3 Transfers
  - 1.6.4 eth Commands
  - 1.6.5 Access via JSON RPC API
  - 1.6.6 Access via Web3
  - 1.6.7 Transaction costs calculation

**2.3.2 Hands-on Lab: Create HelloWorld Smart Contract**

**2.4.2 Hands-on Lab: Create WebShop Smart Contract**

----
## 1.4.3 Hands-on: Install Geth  
**on Mac:**
--

https://github.com/ethereum/go-ethereum/wiki/Installation-Instructions-for-Mac

* start terminal

* Install homebrew
```
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
```

* Install geth
```
brew tap ethereum/ethereum
brew install ethereum
```
(If already installed upgrade perform "brew upgrade ethereum")

**on Windows:**
--

https://github.com/ethereum/go-ethereum/wiki/Installation-instructions-for-Windows

* Download zip file
* extract geth.exe
* start command prompt

```
cmd
```

* change directory 

```
cd <Download Verzeichnis>
```


* start the installation:

```
geth.exe
```

* enter C:\Geth as the installation directory:

```
C:\Geth
```

* add installation path (C:\Geth) in an environment variable:
```
setx path "%path%;C:\Geth"
```


**on Linux:**
--

Source: https://ethereum.github.io/go-ethereum/install/#install-on-ubuntu-via-ppas

* start terminal

* Enable the launchpad repository:

```
sudo add-apt-repository -y ppa:ethereum/ethereum
```

* Install the stable version of Go Ethereum: sudo apt-get update

```
sudo apt-get install ethereum
```

When offline: download the geth tar.gz file and extract Geth under a new directory.



----

###  check the installation and the geth version

```
geth version
```

>Note : Geth release Notes: https://github.com/ethereum/go-ethereum/releases



## Connect to "Main net"

start node on mainNet
```
geth console
```
 
### attach a new console, to run commands

* start new terminal:

**on Mac**

```
geth attach ipc:/Users/XXUSERXX/Library/Ethereum/geth.ipc
```

**On Unix**
```
geth attach ipc:/home/blockchain/.ethereum/geth.ipc
```


* With the admin.peers command in the console you can check to which nodes in the P2P network your own node is currently connected

```
admin.peers
```
> It may take longer for the peers to become visible


example of the output:
```
> admin.peers
[{
    caps: ["eth/63"],
    id: "7c9a204a5f15890d93e623ef9b804e20acf1359db990f37e48299d68597d1e9d07fba2707564f3bc8a524cabb2029bd393626597bcecf4c2b9cedbb0e8d363cd",
    name: "Geth/v1.8.2-stable-b8b9f7f4/windows-amd64/go1.9.2",
    network: {
      inbound: false,
      localAddress: "172.20.10.2:65306",
      remoteAddress: "182.239.246.85:30303",
      static: false,
      trusted: false
    },
    protocols: {
      eth: {
        difficulty: 3.4252593714376836e+21,
        head: "0xfe7deecb60f65ff0a77aed30561211622231b9f9d502d7aa4d1b241eab0762a5",
        version: 63
      }
    }
}]
```

* Check all nodes on the "Main Net" under: https://www.ethernodes.org/network/1


* Cancel nach 1 - 2 Minuten ( Ctrl + D )
(The entire BlockChain data is several GB in size, and is growing daily, details under: https://etherscan.io/chart2/chaindatasizefast )

### Location of blockchain data

Check the blockchain data from the "Main net" on the local machine at the following directory:

**on Mac:**
--
```
/Users/XXXmyUserNameXXX/Library/Ethereum/geth
```

Note: Library is a "hidden" directory,
Hidden files can be displayed via the following 2 terminal "Commands":

1) defaults write com.apple.finder AppleShowAllFiles YES
2) killall Finder


**on Windows:**
--

```
C:\Users\XXXmyUserNameXXX\AppData\Roaming\Ethereum\geth\chaindata
```

Note: AppData is a "hidden" directory, it can be activated via the menu "View - Hidden Files" and the CheckBox in File Explorer


**on Linux:**
--
```
~/.ethereum/geth/nodes
```

----




#  1.5.5 Hands-on: Create a local Blockchain


To close all existing Geth connections:

* exit console

```
exit
```

* close Terminal window

### Create a new directory:

**on Mac:**
--
```
cd ~
mkdir ethereum
cd ethereum
```

**on Windows:**
--
```
md c:\ethereum
cd c:\ethereum
```

**on Linux:**
--
```
cd ~
mkdir ethereum
cd ethereum

```


### Add Path to ENV variables:

**on Mac:**
--
```
echo 'export ethereum_home=/Users/XXXmyUserNameXXX/ethereum' >> ~/.bash_profile
export ethereum_home=/Users/XXXmyUserNameXXX/ethereum
```

( example:
echo 'export ethereum_home=/Users/mbenhajla/Documents/tmp/ethereum' >> ~/.bash_profile
export ethereum_home=/Users/mbenhajla/Documents/tmp/ethereum)


**on Windows:**
--
```
setx ethereum_home "C:\ethereum"
set ethereum_home=C:\ethereum 
echo %ethereum_home%
```

**on Linux:**
--
```
echo 'export ethereum_home=/home/blockchain/ethereum' >> ~/.bashrc 
source ~/.bashrc

```


## Genesis file
### Create the genesis file
**on Mac:**
--
```
touch genesis26.json
// open the file with the linked editor (alternatively TextPad or SublimeText)
open genesis26.json 
```

**on Windows:**
--
```
VisualCode or NotePad++ or SublimeText or ultraEdit
```

**on Linux:**
--
```
touch genesis26.json
// open empty File with VisualCode 
code .
```

### Save the genesis file

Save the following json structure in the file "genesis26.json"
```json
{
  "config": {
        "chainId": 26,
        "homesteadBlock": 0,
        "eip155Block": 0,
        "eip158Block": 0
    },
    "alloc": {},
  "difficulty" : "0x4000",
  "extraData"  : "",
  "gasLimit"   : "0x2fefd8",
  "nonce"      : "0x0000000000000026",
  "mixhash"    : "0x0000000000000000000000000000000000000000000000000000000000000000",
  "parentHash" : "0x0000000000000000000000000000000000000000000000000000000000000000",
  "timestamp"  : "0x00"
}
```

(Optional) Under alloc, pre-funded accounts can be set.
The accounts can be created with Geth beforehand "geth --datadir node1 / account new"

example: 
```
"alloc": {
"0x0000000000000000000000000000000000000001": {"balance": "111111111"},
"0x0000000000000000000000000000000000000002": {"balance": "222222222"}
} 
```

## Init "first block"

**on Mac:**
--
```
geth --datadir $ethereum_home/node1 init $ethereum_home/genesis26.json
```

**on Windows:**
--
```
geth --datadir %ethereum_home%/node1 init %ethereum_home%/genesis26.json
```

**on Linux:**
--
```
geth --datadir $ethereum_home/node1 init $ethereum_home/genesis26.json

```

> Ethereum Genesis Block has 8893 Tx:  https://etherscan.io/txs?block=0 

## Start the node

**on Mac:**
--
```
geth --datadir $ethereum_home/node1 --networkid 26 console
```

**on Windows:**
--
```
geth --datadir %ethereum_home%/node1 --networkid 26 console
```

**on Linux:**
--
```
geth --datadir $ethereum_home/node1 --networkid 26 console
```

Note: Another port can be set using the param "-port" , for example "35555", the default listener port at Geth is 30303



----


# 1.6. Hands-On: Interaction with the Blockchain


## 1.6.1 Interaction via Geth JavaScript console
to list all available commands:

```
eth
```

### Create accounts

```
personal.newAccount()  
```
- Remember and backup the pass phrase
- For real accounts, KeyStore File Backups (found under Node / keyStore)

```
Format  UTC--{Jahr}-{Monat}--{account}
```
This contains the private key encrypted with the pass phrase.

The Private Key may later be removed from the KeyStore and
the pass phrase would be restored, without PassPhrase the key would be unencrypted in the KeyStore
```
{"address":"14631856ad24c58f340fd79ebada5a9348abc035","crypto":{"cipher":"aes-128-ctr","ciphertext":"072d1a7873fdeddf223834bed3adb028709a46aa3aa84ba05d2106c23edfb31b","cipherparams":{"iv":"af73d8460a5c9db02fbe00e2c847c425"},"kdf":"scrypt","kdfparams":{"dklen":32,"n":262144,"p":1,"r":8,"salt":"e7ddebfcb75be169c7a58043855a74ade4bec7d5c795f8e7c53bafe966ce73ba"},"mac":"2ab7d4b81c41c9be5ec56d8bc993e19c5ccc3671aae656d18ba3e71415c19160"},"id":"0c148ffd-e8aa-4c7a-bd50-84bd6b3dacbd","version":3}

```
The public key can be determined from the account (if a transaction is carried out)

> an Ether Walltet does it directly via a UI, Personal. * is only possible locally

### Check accounts

```
eth.accounts
eth.getBalance(eth.accounts[0])
// Hint for Windows on Qwertz (swiss german)
[] Taste = ctrl+alt+ü+!
```



## 1.6.2 Mining 


### Start mining
Mining uses eth.coinbase for mining rewards. eth.coinbase, if not specified is the default eth.accounts [0]

```
// on Console 1
miner.start(1)
```
Note: DAG generation may take longer at the first mining (depending on the set difficulty and the platform, 5-10 minutes or more). DAG file can be found under
/Users/XXUserNameXXX/.ethash
The DAG is needed to calculate the proof of work.

More details regarding DAG can be found under: 
* https://github.com/ethereum/wiki/wiki/Ethash-DAG  
* https://github.com/ethereum/wiki/blob/master/Dagger-Hashimoto.md  


### Attach a second console 
Since the first console is filled by the output from the mining process, a second console should be attached.
In a new terminal window attach neue console über IPC ( in einem neuen Terminal Fenster) :


**on Mac:**
--
```
geth attach ipc:$ethereum_home/node1/geth.ipc
```

**on Windows:**
--
```
geth attach \\.\pipe\geth.ipc
```

**on Linux:**
--
```
geth attach ipc:$ethereum_home/node1/geth.ipc

```
>Note: IPC can be used only locally

### Check number of mined Blocks 

```
// execute following commands on console 2
eth.blockNumber 
```


### Stop mining

As soon as some blocks were mined, we have enough ethers on our default account, and we can stop the mining Prozess.
```
// execute following commands on console 2
miner.stop()
```

```
eth.getBalance(eth.accounts[0])
```



## 1.6.3 Transfers 

Create a second account account2 (with personal.newAccount () analogous to the first account): 

```
personal.newAccount() 
```

and Transfer Ether from account1 to account2 (do not forget mining)

* Unlock Account

```
personal.unlockAccount(eth.accounts[0]) 
```


```
// transfer 2 Ethers = 2000000000000000000 Wei
eth.sendTransaction({ from:eth.accounts[0], to:eth.accounts[1], value: 2000000000000000000})
// (Optional) example with transaction Hash
eth.getTransaction(<Transaction>)
eth.getTransactionReceipt(<Transaction Hash>)
```

> Note: Check if the transaction is still pending. (note the empty block hash if transaction is still pending)

```
eth.pendingTransactions
```

For the transaction to be written, the "miner" process must be started ( see details above)

With eth.getBlock (<Blockhash>), the block with the TrX can be searched.



Units in Ether

```
Unit	                value in Wei
wei                   1 wei	  1
Kwei (babbage)        1e3 wei	  1,000
Mwei (lovelace)       1e6 wei	  1,000,000
Gwei (shannon)        1e9 wei	  1,000,000,000
microether (szabo)    1e12 wei    1,000,000,000,000
milliether (finney)   1e15 wei    1,000,000,000,000,000
ether                 1e18 wei    1,000,000,000,000,000,000
```




## 1.6.4 eth Commands

All available Geth commands can be displayed using:

```
eth
```

Examples:
```
net.version
eth.accounts
eth.mining
eth.blockNumber
eth.getBlock(0)
// Check how much gas was consumed by the transaction
eth.getTransactionReceipt("<txHash>")
eth.getBlock(<Transaction Hash>) 
```

Note: Tapping 2X the "Spacebar" (spaces) and then 2X the "Tab" button, while in the active Geth terminal session, displays also all commands. 


* to exit the console, use
```
exit
```


----

## 1.6.5 Access via JSON RPC API

JSON-RPC is a stateless, light-weight remote procedure call (RPC) protocol. 
It is transport agnostic in that the concepts can be used within the same process, over sockets, over HTTP, or in many various message passing environments. It uses JSON (RFC 4627) as data format

```
Source: https://github.com/ethereum/wiki/wiki/JSON-RPC#json-rpc-api

```

* close terminals 

### Restart node 1 with RPC enabled

* Stop all open terminals

```
exit
```

* Restart node 1 in the new terminal (8545 is the default value for RPC):


**on Mac:**
--
```
geth --datadir $ethereum_home/node1  --rpc --rpcport 8545 --rpcaddr 0.0.0.0 --rpccorsdomain "*" --rpcapi "eth,net,web3" console
```

**on Windows:**
--
```
geth --datadir %ethereum_home%/node1  --rpc --rpcport 8545 --rpcaddr 0.0.0.0 --rpccorsdomain "*" --rpcapi "eth,net,web3" console
```

**on Linux:**
--
```
geth --datadir $ethereum_home/node1  --rpc --rpcport 8545 --rpcaddr 0.0.0.0 --rpccorsdomain "*" --rpcapi "eth,net,web3" console

```

### Execute HTTP Post Request on the blockchain

Open new terminal (console) and run the command "eth_accounts" as an HTTP POST Request via Curl (Client for URLs)

```
curl -X POST -H "Content-Type: application/json" --data '{"jsonrpc":"2.0","method":"eth_accounts","params":[],"id":2}' localhost:8545 -v
```

> example with return value:
{"jsonrpc":"2.0","id":2,"result":["0x685182506beb15c93d4f6ce4a779912ab1857206","0x8527c3b775881168c3825c029418e3a501119bee"]}

> JSON-RPC API are also implemented in Java:
https://github.com/web3j/web3j 




## 1.6.6 Access via Web3
### Web3 library (Dapps, Web Applications)

To talk to an ethereum node from inside a JavaScript application use the web3.js library, which gives a convenient interface for the RPC methods. 

```
Source: https://github.com/ethereum/wiki/wiki/JSON-RPC#javascript-api

```

Web3 is automatically instantiated in the Geth Console. If the Web3 Library is used in an external application, it must be loaded and configured.


#### "Utils" methods

* convert decimal to Hex

```
web3.toHex(123)
```

* convert from wei to Ether// from Ether to Wei

```
web3.toWei(2,"ether")
web3.fromWei(eth.getBalance(eth.accounts[0]), "ether")
// example sendTransaction
// transfer 2 Ether
eth.sendTransaction({ from:eth.accounts[0], to:eth.accounts[1], value: web3.toWei(2,"ether")})
// transfer Ether with a text message 
eth.sendTransaction({from:eth.accounts[0],to:eth.accounts[1],value:web3.toWei(1,'finney'),data:web3.toHex('Ich bin mal Weg!')})

```

#### Block & Mining functions

* Median of the gas price of the last blocks

```
web3.eth.gasPrice
```

#### Show all Web3 methods

> Run in the same console where geth is active
```
web3
```


#### Documentation Web3
https://web3js.readthedocs.io/en/1.0/index.html

#### Exit 

```
exit
```



## 1.6.7 Hands-on Lab: Transaction costs calculation
Each transaction on the Ethereum Blockchain requires gas, and gas has a price.

* Check the Balance of eth.accounts[1]

* Execute a transfer of "1 Ether" from eth.accounts[1] using :
```
eth.sendTransaction({ from:eth.accounts[1], to:eth.accounts[0], value: ...})
```
* Re-Check the Balance of eth.accounts[1]

* calculate how much on our Ethereum Blockchain the transfer of Ether would currently cost in "ether/wei" and how this amount would be in "dollars" ( if we were on the MainNet). Use the following resources to get the results:

  - to check the amount of gas for the Transaction, use the method:

  ```
  eth.getTransactionReceipt("<txHash>")
  ```
  - use https://ethgasstation.info/  to get the current gas price in "ether/wei"
  - use https://etherconverter.online/ to convert the price from  "ether/wei" to dollars.


----
![Pause](../images/Slide_Kaffe.png)



----   

#   2.3.2 Hands-on Lab: Create HelloWorld Smart Contract 



## Online Remix Editor


Online Version: 
http://remix.ethereum.org/

Older Version:
https://ethereum.github.io/browser-solidity/



**on Mac:**
```
Please use Safari or Chrome browser
```
**on Windows:**
```
Please use Chrome Browser (IE will not work)
```
**on Linux:**
```
Please use the Chrome browser
```



## local Remix Editor (optional)

If online is not possible, a local installation of the IDE is **available in the VM (Virtual Machine)**:

```
//Open a terminal ( under the Virtual Machine) and type
remix-ide

//Open a Browser and type URL
http://127.0.0.1:8080

```

Documentation can be found here:
```
https://github.com/ethereum/remix-ide 

```

----


## Create, Deploy and Call a Simple Smart Contract



### Template sample Smart Contract "Greeter"

```

pragma solidity ^0.4.23;

contract Greeting {
    
    //********************************************************************
    // Demo code will be added and explained by the trainer during the //course
    //*******************************************************************

     //  State Variable 
   
    
    //Constructor with initial value
   
    
    // Greet Method
   
    
    // Modify the greet Variable
    
   

    
}

```


----
### Deploy Smart Contract on Blockchain

* exit console if Geth is still running

```
exit
```

#### Restart node1


**on Mac:**
```
geth --datadir $ethereum_home/node1  --rpc --rpcport 8545 --rpcaddr 0.0.0.0 --rpccorsdomain "*" --rpcapi "eth,net,web3" console
```

**on Windows:**
```
geth --datadir %ethereum_home%/node1  --rpc --rpcport 8545 --rpcaddr 0.0.0.0 --rpccorsdomain "*" --rpcapi "eth,net,web3" console
```

**on Linux:**
```
geth --datadir $ethereum_home/node1  --rpc --rpcport 8545 --rpcaddr 0.0.0.0 --rpccorsdomain "*" --rpcapi "eth,net,web3" console

```

> or instead of "*" define the domain as a parameter (so that only the domain "remix.ethereum.org" has access to port 8545)

>geth --datadir $ethereum_home/node1 --rpc --rpcport 8545 --rpcaddr 127.0.0.1 --rpccorsdomain "http://remix.ethereum.org" --rpcapi "eth,net,web3" console

 

### Unlock Account permanently

!! use only for local environment:

```
web3.personal.unlockAccount(eth.accounts[0], "PASSWORT", 0)
```

#### Deploy 
* select web3.provider and enter http: // localhost: 8545

> If localhost does not work on windows (hosts file), use http://127.0.0.1:8545


* Correct Warnings & errors if a newer version is used.

* Click "create" Contract to deploy the contract (with the parameter "Hello World")

Please note that localhost and remix use the same protocol (http or https)

----
![Remix & deploy](../images/remix_create.png)
----

#### Mine the transaction

- Check the pending transaction

```
eth.pendingTransactions
```


-  start mining um den Contract zu deployen
```
miner.start(1)
```

The consumed gas price is deducted from the contract caller's account ("gas limit" is a protection against program bugs)

- execute the functions of the contract via the remix UI
- Check which functions cause a state change, i.e. require a transaction on the chain including mining (gas) and which not.

> In order to check the transaction costs (gas) it is also possible to check the balance before and after the deploy.

> Deploy via Account1 and check Balance in Account1 as the mining rewards are running on Account0
eth.getBalance (eth.accounts [1])


-  stop mining
```
miner.stop()
```

----
#### attach a second console
* Since the first console is filled by the output from the mining process, a second console can be attached.

In a new terminal window:


**on Mac:**

```
geth attach ipc:$ethereum_home/node1/geth.ipc
```

**on Windows:**
```
geth attach \\.\pipe\geth.ipc
or  geth attach http://localhost:8545 ( if IPC on Windows doesn't work)

```
**on Linux:**
```
geth attach ipc:$ethereum_home/node1/geth.ipc

```
* check byteCode of the deployed contract

```
eth.getCode ("<Adress from remix>")

```

> Note: For debugging, the environment must be set to JavaScript VM in the remix (Virtual BC)


----
#  2.4.2 Hands-on Lab: Create WebShop Smart Contract


Our demo shop should support the following 2 functions:

* addProduct (for the shop owner to add products)
* buyProduct (for customers to buy products, and to pay with the standard Ethereum currency)



```
pragma solidity ^0.4.23;

contract Shop { 

    
    // ***************
    //  Model 
    // ***************
    
    // define product struct
	
    
    // add a Map to store all products
    

    // ***************
    //  Permissons 
    // ***************

    // add variable to store the owner

    // define modifier for Owner
	


     // ***************
    // Constructor
    // ***************

    constructor () public {
    	// Set owner equals transaction sender
    	
    }

    // ***************
    //  functions
    // ***************

    // Only owner can add product, Check if Owner
    function addProduct(string _name, uint _uid, uint _price, uint _stock ) public isOwner returns (bool success) {
        
       
    }

    // Anyone can buy product  ( reading and writing to the storage costs )

   function buyProduct(uint _productId)  public payable {
     
            
     }	


}


```



## Create, Deploy and Call a "Shop" Smart Contract

Build your smart contract using the shop contract from the previous demo:

* Copy the edited contract template from the "98-Templates/02-SolidityAndSmartContracts" directory in the Gitlab Repo in the remix editor
* Implement the 2 functions (addProduct, buyProduct):

    - addProduct: First, a product is generated, and then added in the mapping "products".

    - buyProduct: The amount for the purchase of the product is not as a parameter of the function, but will be with the transaction message (msg.value) with delivered, if product available (stock> 0), the stock quantity should be reduced by 1 after the sale.

* (Optional) Implement withdraw function:
    - With this function, the shop owner should transfer the revenue to himself (only available from the shop owner)
 

#### Hints

- Solidity documentation:  http://solidity.readthedocs.io/en/develop/miscellaneous.html
- To deploy the SHOP contract on the local blockchain
    - Select web3.provider in the remix browser, and enter http://localhost:8545
- Use Remix to call and test the Contract functions (add product in shop, buy product)


