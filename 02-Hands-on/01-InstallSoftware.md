# Installation:
----
We have 2 options for the installation of software for the workshop:

* **Option1: Use Linux VM ( For Windows and Mac Users)**: to make it easy to follow the labs and to avoid issues encountered with GETH, we prepared a LINUX virtual machine. 
See Option 1 below for **Installation steps**


* **Option2: Use your own Machine (for Linux Users)**
    - If you have a Linux Laptop you should use your real Laptop machine. (see Option 2 below for **Installation steps**)
   
You need for both options a Laptop with at least 4 GB RAM, to have a decent Performance

# Option1: Use Linux VM

## 1. Install VirtualBox:

* Start the VirtualBox Installation Software from the USB Stick:

**on Mac:**
--
```
VirtualBox-5.2.14-123301-OSX.dmg
```

**on Windows:**
--
```
VirtualBox-5.2.14-123301-Win.exe
```

**on Unix/Linux:**

--

Optional
```
download from: https://www.virtualbox.org/wiki/Downloads , the Version can be newer than the one we use)

```

## 2. Import VM Appliance:

* Start the installed VirtualBox Software

* Select Menu "File -> Import Appliance"

![Import_VM](../images/VirtualBox_ImportApplicance.png)


* Choose USB Stick and select the File "**blockchain-developer-workshop_v1.ova**"

![Import_VM](../images/VirtualBox_ImportApplicance2.png)



## 3. Start VM Appliance:

* Start VM

![Start_VM](../images/VirtualBox_Start.png)

> Note: Ignore the Warning of the missing Shared Folder

## 4. Create Shared Folder:

* create a shared Folder "SharedWithVM" on your Machine ( not inside the VM), to use it for exchanging files between the 2 Computers


----
# Option 2: Use your own Machine ( No VM)
You need to Install Visual Code and NodeJS on your Laptop.

## (optional) Install Visual Code 

Download and follow the Instructions for your OS:
https://code.visualstudio.com/Download
