# Ethereum Developer Workshop

## Learn to Develop with the Ethereum Blockchain

----
### Objective:​
Participants learn how to work with a blockchain and how to implement and deploy self-developed smart contract.
​
### Form:
HandsOn Presence course.
​
### Audience:
Software Developers and managers with technical background who are interested in a fast and efficient introduction to Ethereum
​
### Prerequisites: 
* Basic Programming knowledge.  ( Java or Javascript..)
* Laptop with admin rights.
 
> Uncertain? ask the Trainer mbh@euraconsult.ch

